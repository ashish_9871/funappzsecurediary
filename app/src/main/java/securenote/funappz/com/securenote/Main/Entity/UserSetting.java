package securenote.funappz.com.securenote.Main.Entity;

/**
 * Created by ashish on 30/12/17.
 */

public class UserSetting {
    private int id;
    private String key;
    private String value;

    public UserSetting() {
    }
    public UserSetting( String key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "UserSetting{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
