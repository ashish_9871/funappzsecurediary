package securenote.funappz.com.securenote.Main;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.drive.DriveFile;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import securenote.funappz.com.securenote.BgRunnableTask;
import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DriveBackupHelper;
import securenote.funappz.com.securenote.Helper.GoogleSignInHelper;
import securenote.funappz.com.securenote.Helper.SettingsHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.R;

/**
 * Created by ashish on 21/1/18.
 */

public class SecureNoteService extends Service {
    private AppDBHelper appDBHelper;
    private SettingsHelper settingsHelper;
    private GoogleSignInHelper googleSignInHelper;
    private static int FC = 5001;
    private static int MINUTE_MS = 60000;
    private static int backupInterval = MINUTE_MS * 60 * 6;// 6 hours
    private static int alarmInterval = MINUTE_MS * 30; // 30 minutes

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setAlarm(getApplicationContext());
        appDBHelper = new AppDBHelper(this);
        settingsHelper = SettingsHelper.getInstance(this);
        googleSignInHelper = GoogleSignInHelper.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        try {
            BgRunnableTask backupTask = new BgRunnableTask(new BgRunnableTask.BackgroundProcessable() {
                @Override
                public void execute() {
                    long now = System.currentTimeMillis();
                    long lastBackup = settingsHelper.getLastBackup();
                    if ((now - lastBackup) > backupInterval) {
                        settingsHelper.setLastBackup(now);
                        settingsHelper.setLastBackupStr(now);
                        startBackup(googleSignInHelper.getLastSignInAccount());
                    }
                    stopSelf();
                }

                @Override
                public void onComplete() {

                }
            });
            backupTask.start();
        } catch (Exception e) {
            UtilityHelper.Log(e.getMessage());
        }
        return START_STICKY;
    }

    private void showNotification(String title, String msg) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_icons8_google_drive);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(FC, mBuilder.build());
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void startBackup(GoogleSignInAccount account) {
        if (account == null) {
            settingsHelper.setAccountSetup(false);
            getApplicationContext().sendBroadcast(new Intent(GlobalReceiver.ACTION_GOOGLE_ACCOUNT_GET_FAILED));
            return;
        }
        settingsHelper.setAccountSetup(true);
        String backupData = appDBHelper.notesBackupJson();
        if (backupData == null) {
            return;
        }
        UtilityHelper.storeLocalBackup(getApplicationContext(), backupData);
        DriveBackupHelper.getInstance(getApplicationContext(), account).backupFile(backupData, new DriveBackupHelper.BackupEvents() {
            @Override
            public void onBackupSuccess(DriveBackupHelper driveBackupHelper, DriveFile driveFile) {
                if (canShowNotification()) {
                    showNotification("Secure Diary", "Backup Successful.");
                }
                getApplicationContext().sendBroadcast(new Intent(GlobalReceiver.ACTION_BACKUP_SUCCESS));
                settingsHelper.setLastBackup(System.currentTimeMillis());
                settingsHelper.setLastBackupStr(System.currentTimeMillis());
            }

            @Override
            public void onBackupFailed(DriveBackupHelper driveBackupHelper, Exception e) {
                UtilityHelper.Log(e.getMessage());
                getApplicationContext().sendBroadcast(new Intent(GlobalReceiver.ACTION_BACKUP_FAILED));
            }
        });
    }

    public static void setAlarm(Context context) {
        cancelAlarm(context);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, SecureNoteService.class);
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 60000, alarmInterval, pi);
    }

    private static void cancelAlarm(Context context) {
        Intent intent = new Intent(context, SecureNoteService.class);
        PendingIntent sender = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private boolean canShowNotification() {
        Calendar C = new GregorianCalendar();
        int hour = C.get(Calendar.HOUR_OF_DAY);
        int minute = C.get(Calendar.MINUTE);
        if (hour == 12 && minute > 0 && minute < 20) {
            return true;
        }
        return false;
    }
}
