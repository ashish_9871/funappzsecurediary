package securenote.funappz.com.securenote.Main.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DateHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.Main.Entity.Note;
import securenote.funappz.com.securenote.R;
import securenote.funappz.com.securenote.customview.RichTextView;

/**
 * Created by ashish on 10/11/17.
 */

public class NoteAdapter extends ArrayAdapter<Note> {
    private Context context;
    private int resource;
    private List<Note> notes;
    private AppDBHelper appDBHelper;
    private Note note;
    private SparseBooleanArray selectedNotes;
    private String highlightColor="#eeeeee";

    public NoteAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Note> notes) {
        super(context, resource, notes);
        this.appDBHelper = new AppDBHelper(context);
        this.context = context;
        this.resource = resource;
        this.notes = notes;
        this.selectedNotes = new SparseBooleanArray();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class NoteHolder {
        public TextView noteLabel;
        public RichTextView noteSummary;
        public TextView noteDateDay;
        public TextView noteDateMonth;
        public TextView noteDateYear;
        public ImageButton btnNoteCopy;
        public ImageButton btnNoteShare;
        public ImageButton btnNoteEdit;
    }

    public void addSelectedNote(int position, boolean checked) {
        if (checked) {
            selectedNotes.put(position, checked);
        } else {
            selectedNotes.delete(position);
        }
        notifyDataSetChanged();
    }

    public void clearAllSelected() {
        selectedNotes.clear();
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedNotes() {
        return selectedNotes;
    }

    @Override
    public void remove(@Nullable Note object) {
        notes.remove(object);
        appDBHelper.deleteNote(object.getId());
        notifyDataSetChanged();
    }

    public int getSelectionCount(){
        return selectedNotes.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final int pos = position;
        NoteHolder noteHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(this.resource, parent, false);
            noteHolder = new NoteHolder();
            noteHolder.noteLabel = convertView.findViewById(R.id.noteLabel);
            noteHolder.noteSummary = convertView.findViewById(R.id.noteSummary);
            noteHolder.noteDateDay = convertView.findViewById(R.id.noteDateDay);
            noteHolder.noteDateMonth = convertView.findViewById(R.id.noteDateMonth);
            noteHolder.noteDateYear = convertView.findViewById(R.id.noteDateYear);
            noteHolder.btnNoteShare = convertView.findViewById(R.id.btnNoteShare);
            noteHolder.btnNoteCopy = convertView.findViewById(R.id.btnNoteCopy);
            noteHolder.btnNoteEdit = convertView.findViewById(R.id.btnNoteEdit);
            convertView.setTag(noteHolder);
        } else {
            noteHolder = (NoteHolder) convertView.getTag();
        }

        note = notes.get(pos);
        DateHelper noteDate = DateHelper.getInstance(note.getDate());
        noteHolder.noteLabel.setText(note.getLabel());
        noteHolder.noteSummary.setPlainText(note.getNote());
        noteHolder.noteDateDay.setText(noteDate.getDay());
        noteHolder.noteDateMonth.setText(noteDate.getMonthName());
        noteHolder.noteDateYear.setText(noteDate.getYear());
        noteHolder.btnNoteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GlobalReceiver.ACTION_NOTE_SHARE);
                intent.putExtra(AppDBHelper.KEY_ID, notes.get(pos).getId());
                getContext().sendBroadcast(intent);
            }
        });
        noteHolder.btnNoteCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GlobalReceiver.ACTION_NOTE_COPY);
                intent.putExtra(AppDBHelper.KEY_ID, notes.get(pos).getId());
                getContext().sendBroadcast(intent);
            }
        });
        noteHolder.btnNoteEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GlobalReceiver.ACTION_NOTE_EDIT);
                intent.putExtra(AppDBHelper.KEY_ID, notes.get(pos).getId());
                getContext().sendBroadcast(intent);
            }
        });
        if (selectedNotes.get(pos) && selectedNotes.size() > 0) {
            convertView.setBackgroundColor(Color.parseColor(highlightColor));
        } else {
            convertView.setBackgroundColor(Color.WHITE);
        }

        return convertView;
    }
}