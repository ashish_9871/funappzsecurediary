package securenote.funappz.com.securenote.Helper;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by ashish on 20/1/18.
 */

public class DriveBackupHelper {

    private Context context;
    private DriveResourceClient driveResourceClient;
    private String APP_BACKUP_FILE_MIME = "application/octet-stream";
    private String APP_BACKUP_FILE_NAME = "SecureDiaryData.bkup";
    private GoogleSignInAccount googleSignInAccount;
    private int max_backup = 1;

    private static DriveBackupHelper driveBackupHelperInstance = null;

    public interface BackupEvents {
        void onBackupSuccess(DriveBackupHelper driveBackupHelper, DriveFile driveFile);

        void onBackupFailed(DriveBackupHelper driveBackupHelper, Exception e);
    }

    public interface BackupFetchEvents {
        void onFetchSuccess(DriveBackupHelper driveBackupHelper, String content);

        void onFetchFailure(DriveBackupHelper driveBackupHelper, Exception e);
    }

    public interface SearchEvents {
        void onSuccess(MetadataBuffer metadata);

        void onFailure(Exception e);
    }

    public static DriveBackupHelper getInstance(Context context, GoogleSignInAccount googleSignInAccount) {
        driveBackupHelperInstance = new DriveBackupHelper(context, googleSignInAccount);
        return driveBackupHelperInstance;
    }

    private DriveBackupHelper getInstance() {
        return driveBackupHelperInstance;
    }

    private DriveBackupHelper(Context context, GoogleSignInAccount googleSignInAccount) {
        this.context = context;
        this.googleSignInAccount = googleSignInAccount;
        if (googleSignInAccount != null && context != null) {
            this.driveResourceClient = Drive.getDriveResourceClient(context, googleSignInAccount);
        }
    }

    public void backupFile(final String content, final DriveBackupHelper.BackupEvents events) {
        if (driveResourceClient == null) {
            events.onBackupFailed(getInstance(), new Exception("Invalid Account Details Provided."));
            return;
        }
        searchFile(APP_BACKUP_FILE_NAME, APP_BACKUP_FILE_MIME, false, new SearchEvents() {
            @Override
            public void onSuccess(MetadataBuffer metadata) {
                if (metadata.getCount() > 0) {
                    // write existing file
                    backupAsExistingFile(metadata.get(0).getDriveId().asDriveFile(), Crypto.encrypt(content), events);
                } else {
                    // write new file
                    backupAsNewFile(APP_BACKUP_FILE_NAME, APP_BACKUP_FILE_MIME, Crypto.encrypt(content), events);
                }
            }

            @Override
            public void onFailure(Exception e) {
                events.onBackupFailed(getInstance(), e);
            }
        });


    }


    public void readBackupFile(@NonNull final BackupFetchEvents backupFetchEvents, final boolean syncOnly) {
        if (driveResourceClient == null) {
            if (backupFetchEvents != null) {
                backupFetchEvents.onFetchFailure(getInstance(), new Exception("Please setup backup account first."));
            }
            return;
        }
        searchFile(APP_BACKUP_FILE_NAME, APP_BACKUP_FILE_MIME, false, new SearchEvents() {
            @Override
            public void onSuccess(MetadataBuffer metadata) {
                if (metadata.getCount() > 0) {
                    DriveFile driveFile = metadata.get(0).getDriveId().asDriveFile();
                    Task<DriveContents> openFileTask = driveResourceClient.openFile(driveFile, DriveFile.MODE_READ_ONLY);
                    openFileTask.addOnSuccessListener(new OnSuccessListener<DriveContents>() {
                        @Override
                        public void onSuccess(DriveContents driveContents) {
                            String content = readInputStream(driveContents.getInputStream());
                            content = Crypto.decrypt(content);
                            backupFetchEvents.onFetchSuccess(getInstance(), content);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            backupFetchEvents.onFetchFailure(getInstance(), e);
                        }
                    });
                    return;
                }
                backupFetchEvents.onFetchSuccess(getInstance(), "");
            }

            @Override
            public void onFailure(Exception e) {
                backupFetchEvents.onFetchFailure(getInstance(), e);
            }
        });

    }

    private void backupAsNewFile(final String fileName, final String fileMime, final String content, final BackupEvents events) {
        final Task<DriveFolder> rootFolderTask = driveResourceClient.getRootFolder();
        final Task<DriveContents> createContentsTask = driveResourceClient.createContents();
        Tasks.whenAll(rootFolderTask, createContentsTask).continueWithTask(new Continuation<Void, Task<DriveFile>>() {
            @Override
            public Task<DriveFile> then(@NonNull Task<Void> task) {
                try {
                    DriveContents driveContents = createContentsTask.getResult();
                    OutputStream outputStream = driveContents.getOutputStream();
                    outputStream.write(content.getBytes());
                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(fileName)
                            .setMimeType(fileMime)
                            .build();
                    return driveResourceClient.createFile(rootFolderTask.getResult(), changeSet, driveContents);
                } catch (Exception e) {
                    events.onBackupFailed(getInstance(), e);
                }
                return null;
            }
        }).addOnSuccessListener(new OnSuccessListener<DriveFile>() {
            @Override
            public void onSuccess(DriveFile driveFile) {
                events.onBackupSuccess(getInstance(), driveFile);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                events.onBackupFailed(getInstance(), e);
            }
        });
    }

    private void backupAsExistingFile(final DriveFile driveFile, final String content, final BackupEvents events) {
        Task<DriveContents> openWriteTask = driveResourceClient.openFile(driveFile, DriveFile.MODE_WRITE_ONLY);
        openWriteTask.addOnSuccessListener(new OnSuccessListener<DriveContents>() {
            @Override
            public void onSuccess(DriveContents driveContents) {
                OutputStream os = driveContents.getOutputStream();
                try {
                    os.write(content.getBytes());
                    driveResourceClient.commitContents(driveContents, null);
                    events.onBackupSuccess(getInstance(), driveFile);
                } catch (Exception e) {
                    events.onBackupFailed(getInstance(), e);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                events.onBackupFailed(getInstance(), e);
            }
        });
    }

    private void searchFile(String fileName, String fileMime, boolean trashed, final SearchEvents searchEvents) {
        SortOrder sortOrder = new SortOrder.Builder().addSortDescending(SortableField.MODIFIED_DATE).build();
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, fileMime))
                .addFilter(Filters.eq(SearchableField.TITLE, fileName))
                .addFilter(Filters.eq(SearchableField.TRASHED, trashed))
                .setSortOrder(sortOrder)
                .build();
        Task<MetadataBuffer> queryTask = driveResourceClient.query(query);
        queryTask.addOnSuccessListener(new OnSuccessListener<MetadataBuffer>() {
            @Override
            public void onSuccess(MetadataBuffer metadata) {
                searchEvents.onSuccess(metadata);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                searchEvents.onFailure(e);
            }
        });
    }

    private String readInputStream(InputStream inputStream) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            bufferedReader.close();
            return builder.toString();
        } catch (IOException e) {
            return null;
        }
    }
}
