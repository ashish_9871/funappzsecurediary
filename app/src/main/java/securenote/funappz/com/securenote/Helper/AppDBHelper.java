package securenote.funappz.com.securenote.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import securenote.funappz.com.securenote.Main.Entity.Note;
import securenote.funappz.com.securenote.Main.Entity.UserSetting;

/**
 * Created by ashish on 8/11/17.
 */

public class AppDBHelper extends SQLiteOpenHelper {

    private Context context;
    private String password;
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 12;
    private static final int results_per_page = 5;

    // Database Name
    private static final String DATABASE_NAME = "FUNAPPZ_SECURE_NOTES";

    // Contacts table name
    private static final String TABLE_NOTES = "funappz_notes";
    private static final String TABLE_SETTINGS = "funappz_settings";


    // Notes Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_LABEL = "label";
    public static final String KEY_DATE = "date";
    public static final String KEY_NOTE = "note";
    private static final String KEY_MODIFIED = "last_modified";

    // Settings Table Columns names
    public static final String SETTINGS_KEY_ID = "id";
    public static final String SETTINGS_KEY_KEY = "key";
    public static final String SETTINGS_KEY_VALUE = "value";

    public interface NotesSyncEvents {
        public void onSuccess(int totalSynced);

        public void onFailure(Exception e);
    }

    public AppDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NOTES_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTES + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_LABEL + " TEXT,"
                + KEY_NOTE + " TEXT,"
                + KEY_DATE + " DATE,"
                + KEY_MODIFIED + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                + ")";
        String CREATE_SETTINGS_TABLE = "CREATE TABLE  IF NOT EXISTS " + TABLE_SETTINGS + "("
                + SETTINGS_KEY_ID + " INTEGER PRIMARY KEY,"
                + SETTINGS_KEY_KEY + " VARCHAR(512) UNIQUE,"
                + SETTINGS_KEY_VALUE + " TEXT"
                + ")";

        db.execSQL(CREATE_NOTES_TABLE);
        db.execSQL(CREATE_SETTINGS_TABLE);
    }

    public long saveNote(Note note) {
        String noteString = note.getNote();
        noteString = Crypto.encrypt(noteString);
        note.setNote(noteString);
        if (note.getId() > 0) {
            // update mode
            return updateNote(note);
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LABEL, note.getLabel());
        contentValues.put(KEY_NOTE, note.getNote());
        contentValues.put(KEY_DATE, note.getDate());
        return this.getWritableDatabase().insert(TABLE_NOTES, null, contentValues);
    }

    public Note getNoteBy(String key, String value) {
        Note note = null;
        Cursor cursor = this.getReadableDatabase().query(TABLE_NOTES, null, key + "=?", new String[]{value}, null, null, null, "1");
        if (cursor.moveToFirst()) {
            note = new Note();
            // de
            String noteString = cursor.getString(cursor.getColumnIndex(KEY_NOTE));
            noteString = Crypto.decrypt(noteString);
            note.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            note.setLabel(cursor.getString(cursor.getColumnIndex(KEY_LABEL)));
            note.setNote(noteString);
            note.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
        }
        return note;
    }

    public int deleteNote(int id) {
        String _id = id + "";
        if (_id != null) {
            return this.getWritableDatabase().delete(TABLE_NOTES, "id=?", new String[]{_id});
        }
        return -1;
    }

    private int updateNote(Note note) {
        String id = note.getId() + "";
        if (note.getId() > 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_LABEL, note.getLabel());
            contentValues.put(KEY_NOTE, note.getNote());
            contentValues.put(KEY_DATE, note.getDate());
            return this.getWritableDatabase().update(TABLE_NOTES, contentValues, "id=?", new String[]{id});
        }
        return -1;
    }

    public ArrayList<Note> getNotes(String selection, String[] selectionArgs, String orderBy, String limit) {
        ArrayList<Note> notes = new ArrayList<Note>();
        Cursor cursor = this.getReadableDatabase().query(false, TABLE_NOTES, null, selection, selectionArgs, null, null, orderBy, limit);
        while (cursor.moveToNext()) {
            Note note = new Note();
            // de
            String noteString = cursor.getString(cursor.getColumnIndex(KEY_NOTE));
            noteString = Crypto.decrypt(noteString);
            note.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
            note.setLabel(cursor.getString(cursor.getColumnIndex(KEY_LABEL)));
            note.setNote(noteString);
            note.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
            notes.add(note);
        }
        return notes;
    }

    public ArrayList<Note> getAllNotes() {
        return getNotes(null, null, "id desc", null);
    }

    public ArrayList<Note> getAllNotes(int startFrom) {
        startFrom = startFrom >= 0 ? startFrom : 0;
        String limit = startFrom + "," + results_per_page;
        return getNotes(null, null, "id desc", limit);
    }

    public long getTotalNotes() {
        String sql = "SELECT count(" + KEY_ID + ") as total from " + TABLE_NOTES;
        Cursor cursor = this.getReadableDatabase().rawQuery(sql, null);
        try {
            return Long.parseLong(cursor.getString(0));
        } catch (Exception e) {

        }
        return 0;
    }

    public void syncNotes(String jsonString, @NonNull NotesSyncEvents notesSyncEvents) {
        if (notesSyncEvents == null) {
            notesSyncEvents = new NotesSyncEvents() {
                @Override
                public void onSuccess(int totalSynced) {

                }

                @Override
                public void onFailure(Exception e) {

                }
            };
        }
        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            int totalRecordsSynced = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String label = jsonObject.optString(AppDBHelper.KEY_LABEL, "");
                String description = jsonObject.optString(AppDBHelper.KEY_NOTE, "");
                String date = jsonObject.optString(AppDBHelper.KEY_DATE, DateHelper.getInstance().format(DateHelper.DATE_FORMAT_YMD));
                if (!noteExists(label, description)) {
                    saveNote(new Note(label, date, description));
                    totalRecordsSynced++;
                }
            }
            if (totalRecordsSynced > 0) {
                notesSyncEvents.onSuccess(totalRecordsSynced);
            } else {
                notesSyncEvents.onFailure(new Exception("There are no notes to restore."));
            }
        } catch (JSONException e) {
            notesSyncEvents.onFailure(e);
        }
    }

    public String notesBackupJson() {
        List<Note> notes = getAllNotes();
        JSONArray noteArray = new JSONArray();
        if (notes.size() > 0) {
            for (Note note : notes) {
                noteArray.put(note.toJsonObject());
            }
            return noteArray.toString();
        }
        return null;
    }

    public boolean noteExists(String label, String description) {
        description = Crypto.encrypt(description);
        Cursor cursor = this.getReadableDatabase().query(TABLE_NOTES, null, KEY_LABEL + "=? AND " + KEY_NOTE + "=?", new String[]{label, description}, null, null, null, "1");
        if (cursor.moveToFirst()) {
            return true;
        }
        return false;
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTINGS);
        // Create tables again
    }

    public UserSetting getUserSettingBy(String key, String value) {
        UserSetting userSetting = null;
        Cursor cursor = this.getReadableDatabase().query(TABLE_SETTINGS, null, key + "=?", new String[]{value}, null, null, null, "1");
        if (cursor.moveToFirst()) {
            userSetting = new UserSetting();
            userSetting.setId(cursor.getInt(cursor.getColumnIndex(SETTINGS_KEY_ID)));
            userSetting.setKey(cursor.getString(cursor.getColumnIndex(SETTINGS_KEY_KEY)));
            userSetting.setValue(cursor.getString(cursor.getColumnIndex(SETTINGS_KEY_VALUE)));
        }
        return userSetting;
    }

    public UserSetting getUserSettingById(String value) {
        return getUserSettingBy(SETTINGS_KEY_ID, value);
    }

    public UserSetting getUserSettingByKey(String key) {
        return getUserSettingBy(SETTINGS_KEY_KEY, key);
    }

    public long saveUserSetting(UserSetting userSetting) {
        String id = userSetting.getId() + "";
        String key = userSetting.getKey();
        String value = userSetting.getValue();
        if (key != null) {
            UserSetting existing = getUserSettingByKey(key);
            if (existing != null) { // already exists so update
                userSetting.setId(existing.getId());
                return updateUserSetting(userSetting);
            }
            existing = getUserSettingBy(SETTINGS_KEY_ID, id);
            if (userSetting.getId() > 0) {
                userSetting.setId(existing.getId());
                return updateUserSetting(userSetting);
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(SETTINGS_KEY_KEY, key);
            contentValues.put(SETTINGS_KEY_VALUE, value);
            return this.getWritableDatabase().insert(TABLE_SETTINGS, null, contentValues);
        }
        return -1;
    }

    private long updateUserSetting(UserSetting userSetting) {
        String id = userSetting.getId() + "";
        if (userSetting.getId() > 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(SETTINGS_KEY_KEY, userSetting.getKey());
            contentValues.put(SETTINGS_KEY_VALUE, userSetting.getValue());
            return this.getWritableDatabase().update(TABLE_SETTINGS, contentValues, "id=?", new String[]{id});
        }
        return -1;
    }
}

