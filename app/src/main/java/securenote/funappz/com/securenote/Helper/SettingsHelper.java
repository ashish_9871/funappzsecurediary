package securenote.funappz.com.securenote.Helper;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import securenote.funappz.com.securenote.Main.Entity.UserSetting;
import securenote.funappz.com.securenote.R;

import static android.app.AlarmManager.INTERVAL_DAY;

/**
 * Created by ashish on 24/1/18.
 */

public class SettingsHelper {
    private static final String SETTING_KEY_NO_PASSWORD_LOGIN = "email";
    private static final String SETTING_KEY_EMAIL = "email";
    private static final String SETTING_KEY_APP_KEY = "app_key";
    private static final String SETTING_KEY_COLOR = "color";
    private static final String SETTING_KEY_LAST_RESTORE = "last_restore";
    private static final String SETTING_KEY_LAST_BACKUP = "last_backup";
    private static final String SETTING_KEY_LAST_BACKUP_STR = "last_backup_str";
    private static final String SETTING_KEY_BACKUP_INTERVAL = "backup_interval";
    private static final String SETTING_KEY_AC_SETUP = "account_setup";
    private AppDBHelper appDBHelper;
    private static SettingsHelper settingsHelper;

    private boolean allowedNoPasswordLogin;
    private String email;
    private String color;
    private String appKey;
    private long lastBackup;
    private String lastBackupStr;
    private long lastRestore;
    private long backupInterval;
    private boolean isAccountSetup;

    public static SettingsHelper getInstance(Context context) {
        if (settingsHelper == null) {
            settingsHelper = new SettingsHelper(context);
        }
        return settingsHelper;
    }

    private SettingsHelper(Context context) {
        this.appDBHelper = new AppDBHelper(context);
        // set defaults
        this.color = "#" + Integer.toHexString(ContextCompat.getColor(context, R.color.themeColor));
        this.lastBackup = 0;
        this.lastRestore = 0;
        this.lastBackupStr = "Last Backup: Never";
        this.backupInterval = INTERVAL_DAY;
        this.isAccountSetup = false;
    }

    public boolean isAccountSetup() {
        UserSetting userSetting = appDBHelper.getUserSettingByKey(SETTING_KEY_AC_SETUP);
        if(userSetting!=null){
            return userSetting.getValue().equalsIgnoreCase("true");
        }
        return isAccountSetup;
    }

    public void setAccountSetup(boolean accountSetup) {
        setValue(SETTING_KEY_AC_SETUP,accountSetup+"");
    }

    public String getEmail() {
        UserSetting userSetting = appDBHelper.getUserSettingByKey(SETTING_KEY_EMAIL);
        if (userSetting != null) {
            return userSetting.getValue();
        }
        return email;
    }

    public void setEmail(String email) {
        if (email != null) {
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_EMAIL, email));
        }
    }

    public String getColor() {
        UserSetting userSetting = appDBHelper.getUserSettingByKey(SETTING_KEY_COLOR);
        if (userSetting != null) {
            return userSetting.getValue();
        }
        return color;
    }

    public void setColor(String color) {
        if (color != null) {
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_COLOR, color));
        }
    }

    public String getAppKey() {
        UserSetting userSetting = appDBHelper.getUserSettingByKey(SETTING_KEY_APP_KEY);
        if (userSetting != null) {
            return Crypto.decrypt(userSetting.getValue());
        }
        return appKey;
    }

    public void setAppKey(String appKey) {
        if (appKey != null) {
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_APP_KEY, Crypto.encrypt(appKey)));
        }
    }

    public long getLastRestore() {
        String timestamp = getValue(SETTING_KEY_LAST_RESTORE, null);
        if (timestamp != null) {
            return Long.parseLong(timestamp);
        }
        return lastRestore;
    }

    public void setLastRestore(long timestamp) {
        if (timestamp > 0) {
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_LAST_RESTORE, timestamp + ""));
        }
    }

    public String getLastBackupStr() {
        String str = getValue(SETTING_KEY_LAST_BACKUP_STR, null);
        if(str!=null){
            return str;
        }
        return lastBackupStr;
    }

    public void setLastBackupStr(long ms) {
        if (ms > 0) {
            String str = "Last Backup: "+ DateHelper.getInstance(ms).format(DateHelper.DATE_TIME_FORMAT_HUMAN);
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_LAST_BACKUP_STR, str));
        }
    }

    public long getLastBackup() {
        String timestamp = getValue(SETTING_KEY_LAST_BACKUP, null);
        if (timestamp != null) {
            return Long.parseLong(timestamp);
        }
        return lastBackup;
    }

    public void setLastBackup(long timestamp) {
        if (timestamp > 0) {
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_LAST_BACKUP, timestamp + ""));
        }
    }

    public long getBackupInterval() {
        String interval=getValue(SETTING_KEY_BACKUP_INTERVAL,null);
        try {
            return Long.parseLong(interval);
        }catch (Exception e){

        }
        return backupInterval;
    }

    public void setBackupInterval(long backupInterval) {
        if(backupInterval>=INTERVAL_DAY){
            appDBHelper.saveUserSetting(new UserSetting(SETTING_KEY_BACKUP_INTERVAL,backupInterval+""));
        }
    }

    public boolean isAllowedNoPasswordLogin() {
        String allowed= getValue(SETTING_KEY_NO_PASSWORD_LOGIN,null);
        if(allowed==null){
            return false;
        }
        if(allowed.equals("1")){
            return true;
        }
        return false;
    }

    public void setAllowedNoPasswordLogin(boolean allowedNoPasswordLogin) {
        String value = "0";
        if(allowedNoPasswordLogin){
            value = "1";
        }
        setValue(SETTING_KEY_NO_PASSWORD_LOGIN,value);
    }

    public String getValue(String key, String defaultVal) {
        UserSetting userSetting = appDBHelper.getUserSettingByKey(key);
        if (userSetting != null) {
            return userSetting.getValue();
        }
        return defaultVal;
    }

    public void setValue(String key, String value) {
        if (key != null && value != null) {
            appDBHelper.saveUserSetting(new UserSetting(key, value));
        }
    }
}
