package securenote.funappz.com.securenote.StickyNote;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;

import securenote.funappz.com.securenote.Helper.SettingsHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.R;

/**
 * Implementation of App Widget functionality.
 */
public class StickNoteWidget extends AppWidgetProvider {

    public static final String defaultBG = "#fda242";
    public static final String FLAG_WIDGET_ID = "STICKY_WIDGET_ID";
    public static final String TITLE_PREFIX = "WIDGET_TITLE_";
    public static final String CONTENT_PREFIX = "WIDGET_CONTENT_";
    public static final String COLOR_PREFIX = "WIDGET_COLOR_";
    private int totalWidgets = 0;


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent != null) {
            int id = intent.getIntExtra(FLAG_WIDGET_ID, 0);
            if (id > 0) {
                updateAppWidget(context, AppWidgetManager.getInstance(context), id);
            }
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        totalWidgets = appWidgetIds.length;
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }


    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        // Construct the RemoteViews object
        String title = SettingsHelper.getInstance(context).getValue(getTitleKey(appWidgetId), "Untitled");
        String content = SettingsHelper.getInstance(context).getValue(getContentKey(appWidgetId), "");
        String color = SettingsHelper.getInstance(context).getValue(getColorKey(appWidgetId), defaultBG);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.sticky_note);
        views.setTextViewText(R.id.stickyNoteText, content);
        views.setTextViewText(R.id.stickyNoteTitle, title);
        views.setInt(R.id.stickyNoteContainer, "setBackgroundColor", Color.parseColor(color));
        Intent intent = new Intent(context, StickNoteActivity.class);
        intent.putExtra(FLAG_WIDGET_ID, appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, appWidgetId, intent, 0);
        views.setOnClickPendingIntent(R.id.stickyNoteEdit, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public static String getTitleKey(int widgetId) {
        return TITLE_PREFIX + widgetId;
    }

    public static String getContentKey(int widgetId) {
        return CONTENT_PREFIX + widgetId;
    }

    public static String getColorKey(int widgetId) {
        return COLOR_PREFIX + widgetId;
    }

}

