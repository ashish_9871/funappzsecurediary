package securenote.funappz.com.securenote.customview;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;

import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import securenote.funappz.com.securenote.system.HtmlTagHandler;

/**
 * Created by ashish on 18/11/17.
 */

public class RichEditText extends EmojiconEditText {
    private float maxFontSize = 50;
    private float minFontSize = 5;

    public RichEditText(Context context) {
        super(context);
    }

    public RichEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RichEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setBold() {
        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
        setSpan(styleSpan);
    }

    public void setItalic() {
        StyleSpan styleSpan = new StyleSpan(Typeface.ITALIC);
        setSpan(styleSpan);
    }

    public void setBoldItalic() {
        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD_ITALIC);
        setSpan(styleSpan);
    }

    public void setStrikeThrough() {
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        setSpan(strikethroughSpan);
    }

    public void setUnderline() {
        UnderlineSpan underlineSpan = new UnderlineSpan();
        setSpan(underlineSpan);
    }

    public void setFontSize(float size) {
        if (size <= maxFontSize && size >= minFontSize) {
            setTextSize(size);
        }
    }

    public void setForeGroundColor(int color) {
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(color);
        setSpan(foregroundColorSpan);
    }

    public void setBackGroundColor(int color) {
        BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(color);
        setSpan(backgroundColorSpan);
    }

    public String getHtml() {
        if (Build.VERSION.SDK_INT < 24) {
            return Html.toHtml(getText());
        } else {
            return Html.toHtml(getText(), Html.FROM_HTML_MODE_LEGACY);
        }
    }

    public void setHtml(String htmlText) {
        if (htmlText == null) {
            return;
        }
        if (Build.VERSION.SDK_INT < 24) {
            Spanned spanned = Html.fromHtml(htmlText, null, new HtmlTagHandler());
            setText(spanned);
        } else {
            Spanned spanned = Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY, null, new HtmlTagHandler());
            setText(spanned);
        }
    }

    public void addTextCurrentPosition(String text) {
        getText().insert(getSelectionStart(), text);
    }

    private void setSpan(Object spanned) {
        SpannableString spannableString = new SpannableString(getText());
        spannableString.setSpan(spanned, getSelectionStart(), getSelectionEnd(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(spannableString);
    }
}
