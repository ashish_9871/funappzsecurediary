package securenote.funappz.com.securenote.Helper;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import securenote.funappz.com.securenote.BgRunnableTask;
import securenote.funappz.com.securenote.Main.Entity.Note;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ashish on 7/11/17.
 */

public class UtilityHelper {

    private static String localBackupFile = "securediarybackup.bkup";
    private static String salt = "12345678";
    private static int numIteration = 100;
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String authFile = "__APPAUTH__";

    private static String encryptionAlgo = "PBEWithMD5AndDES";
    private static Calendar calendar = new GregorianCalendar();

    private Context context;


    public UtilityHelper(Context context) {
        this.context = context;
    }


    public static void showKeyBoard(Window window) {
        View view = window.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) window.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            view.requestFocus();
            imm.showSoftInput(view, 0);
        }
    }


    public static void Log(String message) {
        Log.e("__LOG", message + "");
    }

    public static void insertFakeNotes(final AppDBHelper appDBHelper, final int count) {
        /*new BgRunnableTask(new BgRunnableTask.BackgroundProcessable() {

            @Override
            public void execute() {
                long total = appDBHelper.getTotalNotes();
                if (total <= count) {
                    String date = DateHelper.getInstance().format(DateHelper.DATE_FORMAT_YMD);
                    for (int i = (count - (int) total); i >= 0; i--) {
                        appDBHelper.saveNote(new Note(i + "", date, "DESC-" + i));
                    }
                }
            }

            @Override
            public void onComplete() {

            }
        }).start();*/

    }

    public static boolean storeLocalBackup(Context context, String data) {
        try {
            data = Crypto.encrypt(data);
            FileOutputStream fos = context.openFileOutput(localBackupFile, MODE_PRIVATE);
            fos.write(data.getBytes());
            fos.close();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public static String getLocalBackup(Context context) {
        String data = null;
        try {
            FileInputStream fis = context.openFileInput(localBackupFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            bufferedReader.close();
            data = builder.toString();
            data = Crypto.decrypt(data);
        } catch (Exception e) {
            UtilityHelper.Log(e.getMessage());
        }
        return data;
    }


}
