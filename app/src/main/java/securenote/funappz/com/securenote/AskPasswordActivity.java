package securenote.funappz.com.securenote;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.Main.MainAppActivity;

public class AskPasswordActivity extends BaseActivity implements View.OnClickListener {

    private TextView logoText;
    private TextView logoTagline;
    private EditText passwordInput;
    private Button btnUnlock;
    private ImageView btnForget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_password);
        logoText = (TextView) findViewById(R.id.logoText);
        logoTagline = (TextView) findViewById(R.id.logoTagline);
        passwordInput = (EditText) findViewById(R.id.passwordInput);
        btnUnlock = (Button) findViewById(R.id.btnUnlock);
        btnUnlock.setOnClickListener(this);
        btnForget = (ImageView) findViewById(R.id.btnForget);
        btnForget.setOnClickListener(this);
        logoText.setTypeface(BaseActivity.TypeFacepacifico);
        logoTagline.setTypeface(BaseActivity.TypeFacetangerine);
        passwordInput.clearFocus();
    }


    @Override
    public void onBackPressed() {
        passwordInput.clearFocus();
        exitOnDoubleBackPress();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnForget:
                passwordInput.clearFocus();
                passwordInput.setError(null);
                hideKeyBoard();
                buildConfirmDialog("Get Password", "Password will be sent to your registered email.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new BgRunnableTask(new BgRunnableTask.BackgroundProcessable() {
                            @Override
                            public void execute() {
                                String email = getSettingsHelper().getEmail();
                                String password = getSettingsHelper().getAppKey();
                                try {
                                    JSONObject jsonObject = getApiHelper().sendForgetPasswordRequest(email, password);
                                } catch (Exception e) {
                                    UtilityHelper.Log(e.getMessage());
                                }
                            }

                            @Override
                            public void onComplete() {
                            }
                        }).start();
                        String msg = "Password Sent to your registered email.";
                        onSuccess(msg);
                        showToast(msg, true);
                    }
                }, null);
                break;
            case R.id.btnUnlock:
                String password = passwordInput.getText().toString();
                if (password.isEmpty()) {
                    passwordInput.setError("Please enter password");
                } else if (!getSettingsHelper().getAppKey().equalsIgnoreCase((password))) {
                    passwordInput.setError("Invalid Password.");
                    return;
                } else {
                    Intent mainApp = new Intent(getApplicationContext(), MainAppActivity.class);
                    mainApp.setAction(GlobalReceiver.ACTION_PASSWORD_VERIFIED);
                    startActivity(mainApp);
                    onSuccess("Success!!");
                    finish();
                }
        }
    }

    public void onSuccess(String msg) {
        showSnakeBar(msg, false);
    }
}
