package securenote.funappz.com.securenote.Main.Fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DateHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.Main.Entity.Note;
import securenote.funappz.com.securenote.Main.MainAppActivity;
import securenote.funappz.com.securenote.R;
import securenote.funappz.com.securenote.customview.RichEditText;


/**
 * Created by ashish on 4/1/18.
 */

public class NoteAddFragment extends DialogFragment {

    private EditText noteLabel;
    private RichEditText noteEditor;
    private ImageButton noteDateHandler;
    private EditText noteDate;
    private ImageButton btnSmileyPicker;
    private ImageButton btnNoteBold;
    private ImageButton btnNoteItalic;
    private ImageButton btnNoteStrikeThrough;
    private LinearLayout btnNoteSave;
    private Note note;
    private DatePickerDialog datePickerDialog;
    private DateHelper dateHelper;
    private FrameLayout smileyKeyBoardFrame;
    private EmojIconActions emojIconActions;
    private boolean showSmiley = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_note_add, container, false);
        noteLabel = view.findViewById(R.id.noteLabel);
        noteDateHandler = view.findViewById(R.id.noteDateHandler);
        noteDate = view.findViewById(R.id.noteDate);
        noteEditor = view.findViewById(R.id.noteEditor);
        btnSmileyPicker = view.findViewById(R.id.btnSmileyPicker);
        btnNoteBold = view.findViewById(R.id.btnNoteBold);
        btnNoteItalic = view.findViewById(R.id.btnNoteItalic);
        btnNoteStrikeThrough = view.findViewById(R.id.btnStrikeThrough);
        btnNoteSave = view.findViewById(R.id.btnNoteSave);
        smileyKeyBoardFrame = view.findViewById(R.id.smileyKeyBoardFrame);
        dateHelper = DateHelper.getInstance();


        if (getArguments() != null) {
            boolean isCopyAction = getArguments().getBoolean(GlobalReceiver.ACTION_NOTE_COPY, false);
            int id = getArguments().getInt(AppDBHelper.KEY_ID);
            note = getMainAppActivity().getAppDBHelper().getNoteBy(AppDBHelper.KEY_ID, id + "");
            if (isCopyAction && note != null) {
                note.setId(0);
                note.setLabel("Copy of " + note.getLabel());
            }
        }
        note = (note == null ? new Note() : note);
        if (note.getDate() == null) {
            // set current date if note date is null
            note.setDate(dateHelper.format(DateHelper.DATE_FORMAT_YMD));
        }
        noteLabel.setText(note.getLabel());
        noteLabel.clearFocus();
        noteEditor.setHtml(note.getNote());
        noteLabel.clearFocus();
        noteDate.setText(DateHelper.getInstance(note.getDate()).format(DateHelper.DATE_FORMAT_HUMAN));
        setActionDatePicker();
        setActionBold();
        setActionItalic();
        setActionStrikeThrough();
        setActionSave();
        emojIconActions = new EmojIconActions(getMainAppActivity().getApplicationContext(), view, noteEditor, btnSmileyPicker);
        emojIconActions.setUseSystemEmoji(false);
        getMainAppActivity().setTitleAndBackButton((note.getId() > 0 ? R.string.note_edit_screen_title : R.string.note_add_screen_title), true);
        noteLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojIconActions.closeEmojIcon();
            }
        });
        noteLabel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                emojIconActions.closeEmojIcon();
            }
        });
        noteEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showSmiley) {
                    emojIconActions.ShowEmojIcon();
                    showSmiley = false;
                } else {
                    emojIconActions.closeEmojIcon();
                }
            }
        });
        noteEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    noteEditor.callOnClick();
                }
            }
        });
        noteDateHandler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojIconActions.closeEmojIcon();
                noteDate.callOnClick();
            }
        });
        return view;
    }

    private void setActionDatePicker() {
        noteDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog = new DatePickerDialog(view.getRootView().getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        dateHelper = DateHelper.getInstance(day, month, year);
                        if (dateHelper != null) {
                            note.setDate(dateHelper.format(DateHelper.DATE_FORMAT_YMD));
                            noteDate.setText(dateHelper.format(DateHelper.DATE_FORMAT_HUMAN));
                        }
                    }
                }, dateHelper.getYearInt(), dateHelper.getMonthInt(), dateHelper.getDayInt());
                datePickerDialog.setTitle(null);
                datePickerDialog.show();
            }
        });

    }

    private void setActionBold() {
        btnNoteBold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteEditor.setBold();
            }
        });
    }

    private void setActionItalic() {
        btnNoteItalic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteEditor.setItalic();
            }
        });
    }

    private void setActionStrikeThrough() {
        btnNoteStrikeThrough.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noteEditor.setStrikeThrough();
            }
        });
    }

    private void setActionSave() {
        btnNoteSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMainAppActivity().hideKeyBoard();
                if (noteLabel.getText().toString().isEmpty()) {
                    noteLabel.setError("Note Label is required.");
                    noteLabel.requestFocus();
                } else if (noteEditor.getText().toString().isEmpty()) {
                    noteEditor.setError("Note is required.");
                    noteEditor.requestFocus();
                } else if (noteDate.getText().toString().isEmpty()) {
                    noteDate.setError("Date is required.");
                    noteDate.requestFocus();
                } else {
                    note.setLabel(noteLabel.getText().toString());
                    note.setNote(noteEditor.getHtml());
                    if (getMainAppActivity().getAppDBHelper().saveNote(note) != -1) {
                        getMainAppActivity().showSnakeBar("Diary Note Saved.", true);
                        ((MainAppActivity) getActivity()).showHomeFragment(null);
                    } else {
                        getMainAppActivity().showSnakeBar("Diary Note can't be saved.", true);
                    }
                }
            }
        });
    }

    private MainAppActivity getMainAppActivity() {
        return (MainAppActivity) getActivity();
    }
}
