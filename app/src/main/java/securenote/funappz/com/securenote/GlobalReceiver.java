package securenote.funappz.com.securenote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.Main.SecureNoteService;

/**
 * Created by ashish on 22/1/18.
 */

public class GlobalReceiver extends BroadcastReceiver {
    public static final String ACTION_PASSWORD_SETUP = "funappz.APP_PASSWORD_SETUP";
    public static final String ACTION_PASSWORD_VERIFIED = "funappz.APP_PASSWORD_VERIFIED";
    public static final String ACTION_NOTE_LIST = "funappz.SECURE_NOTE_LIST";
    public static final String ACTION_NOTE_ADD = "funappz.SECURE_NOTE_ADD";
    public static final String ACTION_NOTE_COPY = "funappz.SECURE_NOTE_COPY";
    public static final String ACTION_NOTE_SHARE = "funappz.SECURE_NOTE_SHARE";
    public static final String ACTION_NOTE_EDIT = "funappz.SECURE_NOTE_EDIT";
    public static final String ACTION_NOTE_DELETE = "funappz.SECURE_NOTE_DELETE";
    public static final String ACTION_NOTE_SAVED = "funappz.SECURE_NOTE_SAVED";
    public static final String ACTION_CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String ACTION_GOOGLE_ACCOUNT_GET_FAILED = "funappz.GOOGLE_ACCOUNT_GET_FAILED";
    public static final String ACTION_BACKUP_SUCCESS = "funappz.BACKUP_SUCCESS";
    public static final String ACTION_BACKUP_FAILED = "funappz.BACKUP_FAILED";
    public static final String ACTION_RESTORE_SUCCESS = "funappz.RESTORE_SUCCESS";
    public static final String ACTION_RESTORE_FAILED = "funappz.RESTORE_FAILED";

    private Context context;
    private IIntentListener iIntentListener;

    public interface IIntentListener {
        void onIntentReceive(Intent intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent != null) {
            String action = intent.getAction();
            switch (action) {
                case Intent.ACTION_BOOT_COMPLETED:
                    context.startService(new Intent(context, SecureNoteService.class));
                    break;
                case ACTION_CONNECTIVITY_CHANGE:
                    ConnectivityManager connectivityManager
                            = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    Constants.internetConnected = (activeNetworkInfo != null && activeNetworkInfo.isConnected());
                    UtilityHelper.Log("Network change:-" + Constants.internetConnected);
                    break;
            }
        }
    }
}

