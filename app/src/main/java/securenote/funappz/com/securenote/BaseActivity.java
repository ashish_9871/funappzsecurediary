package securenote.funappz.com.securenote;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import securenote.funappz.com.securenote.Helper.ApiHelper;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DriveBackupHelper;
import securenote.funappz.com.securenote.Helper.GoogleSignInHelper;
import securenote.funappz.com.securenote.Helper.SettingsHelper;
import securenote.funappz.com.securenote.Main.SecureNoteService;
import securenote.funappz.com.securenote.customview.ColorPickerDialog;

public class BaseActivity extends AppCompatActivity {
    private boolean doubleBackToExitPressedOnce = false;
    private static boolean startedService = false;
    private AppDBHelper appDBHelper;
    private SettingsHelper settingsHelper;
    private ApiHelper apiHelper;
    private GoogleSignInAccount googleSignInAccount;
    private DriveBackupHelper driveBackupHelper;
    private GoogleSignInHelper googleSignInHelper;
    private Dialog progressBarDialog;
    private AlertDialog confirmDialog;
    private Snackbar snackbar;
    public static Typeface TypeFacepacifico;
    public static Typeface TypeFacecinzel;
    public static Typeface TypeFacetangerine;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingsHelper = SettingsHelper.getInstance(getApplicationContext());
        apiHelper = ApiHelper.getInstance();
        appDBHelper = new AppDBHelper(getApplicationContext());
        progressBarDialog = new Dialog(this);
        progressBarDialog.setCancelable(false);
        ProgressBar progressBar = new ProgressBar(getApplicationContext());
        progressBar.setLayoutParams(getWindow().getAttributes());
        progressBar.setBackgroundColor(Color.TRANSPARENT);
        progressBarDialog.addContentView(progressBar, getWindow().getAttributes());
        TypeFacepacifico = Typeface.createFromAsset(getAssets(), Constants.FONT_FILE_PACIFICO);
        TypeFacecinzel = Typeface.createFromAsset(getAssets(), Constants.FONT_FILE_CINZEL);
        TypeFacetangerine = Typeface.createFromAsset(getAssets(), Constants.FONT_FILE_TANGERINE);
        if (!startedService) {
            startedService = true;
            startService(new Intent(getApplicationContext(), SecureNoteService.class));
        }
    }

    public void exitOnDoubleBackPress() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        showToast("Please back again to exit.", false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void buildConfirmDialog(String successButtonText, String msg, DialogInterface.OnClickListener successListener, DialogInterface.OnClickListener failListener) {
        if(confirmDialog!=null){
            confirmDialog.cancel();
        }
        confirmDialog=new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(null)
                .setMessage(msg)
                .setPositiveButton(successButtonText, successListener)
                .setNegativeButton("Cancel", (failListener != null ? failListener : new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })).show();
    }

    public void buildColorDialog(String color, ColorPickerDialog.OnColorSelectListener colorSelectListener) {
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog(this);
        colorPickerDialog.setColor(color);
        colorPickerDialog.onColorChoose(colorSelectListener).show();
    }

    public void showProgressBarDialog(boolean show) {
        if (show) {
            progressBarDialog.show();
        } else {
            progressBarDialog.hide();
        }
    }

    public void hideKeyBoard() {
        View view = getWindow().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getWindow().getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showToast(String msg, boolean londuration) {
        Toast.makeText(getApplicationContext(), msg + "", (londuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT)).show();
    }

    public void showSnakeBar(String msg, boolean londuration) {
        snackbar = Snackbar.make(getWindow().getDecorView(), msg, (londuration ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT));
        snackbar.getView().setBackgroundResource(R.color.themeColor);
        snackbar.show();
    }

    public void setTitleAndBackButton(int titleRes, boolean backButtonEnable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleRes);
            getSupportActionBar().setDisplayHomeAsUpEnabled(backButtonEnable);
            getSupportActionBar().setDisplayShowHomeEnabled(backButtonEnable);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }

    protected void init(){
        googleSignInHelper = GoogleSignInHelper.getInstance(getApplicationContext());
        googleSignInAccount = googleSignInHelper.getLastSignInAccount();
        driveBackupHelper = DriveBackupHelper.getInstance(getApplicationContext(), googleSignInAccount);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public boolean isDiarySetup() {
        boolean setup = (settingsHelper.getEmail() != null && settingsHelper.getAppKey() != null);
        return setup;
    }

    public boolean isAllowedNoPassword(){
        return settingsHelper.isAllowedNoPasswordLogin();
    }

    public AppDBHelper getAppDBHelper() {
        return appDBHelper;
    }

    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    public SettingsHelper getSettingsHelper() {
        return settingsHelper;
    }

    public GoogleSignInAccount getGoogleSignInAccount() {
        return googleSignInAccount;
    }

    public DriveBackupHelper getDriveBackupHelper() {
        return driveBackupHelper;
    }

    public GoogleSignInHelper getGoogleSignInHelper() {
        return googleSignInHelper;
    }
}
