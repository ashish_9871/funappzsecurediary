package securenote.funappz.com.securenote.Main.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Main.Adapter.NoteAdapter;
import securenote.funappz.com.securenote.Main.Entity.Note;
import securenote.funappz.com.securenote.Main.MainAppActivity;
import securenote.funappz.com.securenote.R;

/**
 * Created by ashish on 4/1/18.
 */

public class HomeFragment extends Fragment {

    private final int lazyLoadThreshold = 1;
    private ListView notesListing;
    private TextView noNotesText;
    private NoteAdapter noteAdapter;
    private ArrayList<Note> notes;
    private ImageButton noteAddButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes_layout, container, false);
        noNotesText = view.findViewById(R.id.noNotesText);
        notesListing = view.findViewById(R.id.notesListing);
        noteAddButton = view.findViewById(R.id.noteAddButton);
        noteAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(GlobalReceiver.ACTION_NOTE_ADD);
                getMainAppActivity().getApplicationContext().sendBroadcast(intent);
            }
        });

        notesListing.setItemsCanFocus(true);
        notesListing.setScrollingCacheEnabled(false);
        //
        notesListing.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        notesListing.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                noteAdapter.addSelectedNote(i, b);
                actionMode.setTitle(noteAdapter.getSelectionCount() + " selected");
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                noteAdapter.clearAllSelected();
                actionMode.getMenuInflater().inflate(R.menu.delete_action_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(final ActionMode actionMode, MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.action_delete_selected:
                        getMainAppActivity().buildConfirmDialog("Continue", "Selected notes will be deleted.", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SparseBooleanArray selectedNotes = noteAdapter.getSelectedNotes();
                                for (int j = (selectedNotes.size() - 1); j >= 0; j--) {
                                    if (selectedNotes.valueAt(j)) {
                                        Note note = noteAdapter.getItem(selectedNotes.keyAt(j));
                                        noteAdapter.remove(note);
                                    }
                                }
                                getMainAppActivity().showSnakeBar("Deleted Sucessfully.", true);
                                actionMode.finish();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                actionMode.finish();
                            }
                        });
                        break;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                noteAdapter.clearAllSelected();
            }
        });


        notes = getMainAppActivity().getAppDBHelper().getAllNotes(0);
        getMainAppActivity().setTitleAndBackButton(R.string.title_activity_main_app, false);
        noteAdapter = new NoteAdapter(getActivity().getApplicationContext(), R.layout.note_list_item, notes);
        //
        notesListing.setAdapter(noteAdapter);
        noteAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                observerListEmpty();
            }
        });
        noteAdapter.notifyDataSetChanged();
        notesListing.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean cond = ((firstVisibleItem + visibleItemCount) >= (totalItemCount - lazyLoadThreshold));
                if (cond) {
                    notesListing.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ArrayList<Note> notesAppend = getMainAppActivity().getAppDBHelper().getAllNotes(notes.size());
                            if (notesAppend.size() > 0) {
                                notes.addAll(notesAppend);
                                noteAdapter.notifyDataSetChanged();
                            }
                        }
                    }, 50);
                }
            }
        });

        return view;
    }

    private void observerListEmpty() {
        notesListing.setVisibility(View.GONE);
        noNotesText.setVisibility(View.GONE);

        if (notes.size() > 0) {
            notesListing.setVisibility(View.VISIBLE);
        } else {
            noNotesText.setVisibility(View.VISIBLE);
        }
    }

    private MainAppActivity getMainAppActivity() {
        return ((MainAppActivity) getActivity());
    }
}
