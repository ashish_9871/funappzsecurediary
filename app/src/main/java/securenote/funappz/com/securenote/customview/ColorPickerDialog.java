package securenote.funappz.com.securenote.customview;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import securenote.funappz.com.securenote.R;

/**
 * Created by ashish on 9/1/18.
 */

public class ColorPickerDialog extends AlertDialog.Builder implements SeekBar.OnSeekBarChangeListener {
    private Context context;
    private LinearLayout layout;
    private TextView colorDisplay;
    private TextView colorValueDisplay;
    private SeekBar Red;
    private SeekBar Green;
    private SeekBar Blue;
    private SeekBar Alpha;
    private int r = 255;
    private int g = 0;
    private int b = 0;
    private int a = 255;

    public interface OnColorSelectListener{
        void onSelect(String colorHex, DialogInterface dialogInterface, int i);
    }
    public ColorPickerDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public AlertDialog show() {
        View view = LayoutInflater.from(context).inflate(R.layout.color_picker_dialog, null);
        colorDisplay = view.findViewById(R.id.colorDisplay);
        colorValueDisplay = view.findViewById(R.id.colorValueDisplay);

        Red = view.findViewById(R.id.redSeeker);
        Red.setMax(255);
        Red.setOnSeekBarChangeListener(this);
        Red.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        Red.getThumb().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        Red.setProgress(r);

        Green = view.findViewById(R.id.greeSeeker);
        Green.setMax(255);
        Green.setOnSeekBarChangeListener(this);

        Green.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
        Green.getThumb().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
        Green.setProgress(g);

        Blue = view.findViewById(R.id.blueSeeker);
        Blue.setMax(255);
        Blue.setOnSeekBarChangeListener(this);

        Blue.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
        Blue.getThumb().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
        Blue.setProgress(b);

        Alpha = view.findViewById(R.id.alphaSeeker);
        Alpha.setMax(255);
        Alpha.setOnSeekBarChangeListener(this);
        Alpha.setProgress(a);
        Alpha.setVisibility(View.GONE);
        setNegativeButton("Cancel",null);
        setView(view);
        return super.show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean bool) {

        switch (seekBar.getId()) {
            case R.id.redSeeker:
                r = progress;
                break;
            case R.id.greeSeeker:
                g = progress;
                break;
            case R.id.blueSeeker:
                b = progress;
                break;
            case R.id.alphaSeeker:
                a = progress;
                break;
        }
        onColorSelect();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public String selectedColorHex() {
        String rr= Integer.toHexString(this.r) ;
        rr = (rr.length()<2 ? "0"+rr : rr);
        String gg= Integer.toHexString(this.g);
        gg= (gg.length()<2 ? "0"+gg : gg);
        String bb = Integer.toHexString(this.b);
        bb = (bb.length()<2 ? "0"+bb : bb);
        return "#"+rr+gg+bb;

    }

    public AlertDialog.Builder onColorChoose(final OnColorSelectListener listener) {
        return setPositiveButton("Choose Color", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onSelect(selectedColorHex(),dialogInterface,i);
            }
        });
    }

    @Override
    public AlertDialog.Builder setPositiveButton(@StringRes int textId, DialogInterface.OnClickListener listener)  {
        return this;
    }

    private void onColorSelect() {
        int color = Color.argb(a, r, g, b);
        colorDisplay.setBackgroundColor(color);
        colorDisplay.setTextColor(color);

        String hexString = selectedColorHex();
        colorValueDisplay.setText(hexString);
    }

    public void setColor(int r, int g, int b) {
        this.r = getValidColorInt(r);
        this.g = getValidColorInt(g);
        this.b = getValidColorInt(b);
        onColorSelect();
    }

    public void setColor(String hexString){
        hexString = hexString.charAt(0)=='#' ? hexString.substring(1,hexString.length()) : hexString;
        hexString = hexString.length()>6 ? hexString.substring(0,5) : hexString;
        int color = Integer.parseInt(hexString, 16);
        this.r = (color >> 16) & 0xFF;
        this.g = (color >> 8) & 0xFF;
        this.b = (color >> 0) & 0xFF;
    }


    public static String getInvertedColor(String hexString) {
        hexString = hexString.charAt(0) == '#' ? hexString.substring(1, hexString.length()) : hexString;
        hexString = hexString.length() > 6 ? hexString.substring(0, 5) : hexString;
        int color = Integer.parseInt(hexString, 16);
        int r = 255 - (color >> 16) & 0xFF;
        int g = 255 - (color >> 8) & 0xFF;
        int b = 255 - (color >> 0) & 0xFF;
        return "#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);
    }

    private int getValidColorInt(int i) {
        if (i <= 255 && i >= 0) {
            return i;
        }
        return 255;
    }
}
