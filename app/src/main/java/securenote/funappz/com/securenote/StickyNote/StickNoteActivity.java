package securenote.funappz.com.securenote.StickyNote;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import securenote.funappz.com.securenote.BaseActivity;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.R;
import securenote.funappz.com.securenote.customview.RichEditText;

public class StickNoteActivity extends BaseActivity {

    private Intent intent;
    private int widgetId;
    private EditText noteLabel;
    private RichEditText noteEditor;
    private String[] colors = new String[]{
            "#fda242", "#8af744", "#e9c0c8", "#98e1c4", "#fd80ac",
            "#edef4c", "#dfeed1", "#fec26b", "#7ad1e4", "#9f9ffb",
            "#fdbadb", "#d6e87c", "#fdbf82"
    };
    private RecyclerView stickNoteBgColorPicker;
    private ColorPickerAdapter colorPickerAdapter;
    private String userColor = StickNoteWidget.defaultBG;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sticky_note_editor);
        setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        intent = getIntent();
        if (intent != null) {
            widgetId = intent.getIntExtra(StickNoteWidget.FLAG_WIDGET_ID, 0);
            if (widgetId == 0) {
                finish();
                return;
            }
            noteLabel = findViewById(R.id.noteLabel);
            noteEditor = findViewById(R.id.noteEditor);
            linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            colorPickerAdapter = new ColorPickerAdapter();
            stickNoteBgColorPicker = findViewById(R.id.stickNoteBgColorPicker);
            stickNoteBgColorPicker.setLayoutManager(linearLayoutManager);
            stickNoteBgColorPicker.setAdapter(colorPickerAdapter);
            stickNoteBgColorPicker.addOnItemTouchListener(new RecyclerViewItemClickListener(getApplicationContext(), new RecyclerViewItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    userColor = colors[position];
                    colorPickerAdapter.setSelected(position);
                }
            }));
            colorPickerAdapter.notifyDataSetChanged();
            final String title = getSettingsHelper().getValue(StickNoteWidget.getTitleKey(widgetId), "Untitled");
            final String content = getSettingsHelper().getValue(StickNoteWidget.getContentKey(widgetId), "");
            final String color = getSettingsHelper().getValue(StickNoteWidget.getColorKey(widgetId), userColor);
            try {
                noteLabel.setText(title);
                noteEditor.setText(content);
                stickNoteBgColorPicker.scrollToPosition(selectColor(color));
            } catch (Exception ex) {

            }

        }
    }

    private int selectColor(String colorHex){
        int position =0;
        for (int i=0;i<colors.length;i++){
            if(colorHex.equalsIgnoreCase(colors[i])){
                position = i;
                colorPickerAdapter.setSelected(i);
                break;
            }
        }
        return position;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sticky_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save_sticky_note:
                String title = noteLabel.getText().toString().trim();
                String content = noteEditor.getText().toString().trim();
                if (title.isEmpty()) {
                    noteLabel.setError("Sticky Note Title is Required.");
                    return true;
                }
                getSettingsHelper().setValue(StickNoteWidget.getTitleKey(widgetId), title);
                getSettingsHelper().setValue(StickNoteWidget.getContentKey(widgetId), content);
                getSettingsHelper().setValue(StickNoteWidget.getColorKey(widgetId), userColor);
                showToast("Sticky note saved.", false);
                broadCastWidgetData();
                break;
            case android.R.id.home:
                onBackPressed();
                break;

        }
        return true;
    }

    private void broadCastWidgetData() {
        intent = new Intent(this, StickNoteWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(StickNoteWidget.FLAG_WIDGET_ID, widgetId);
        sendBroadcast(intent);
        onBackPressed();
        moveTaskToBack(true);
    }


    private class ColorPickerAdapter extends RecyclerView.Adapter<ColorPickerAdapter.ColorPickerViewHolder> {

        private int selected = 0;

        @Override
        public ColorPickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.color_block, parent, false);
            return new ColorPickerViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ColorPickerViewHolder holder, int position) {
            holder.colorBox.setImageDrawable(null);
            if (position == selected) {
                holder.colorBox.setImageResource(R.drawable.ic_check_black_24dp);
            }
            holder.colorBox.setBackgroundColor(Color.parseColor(colors[position]));
        }

        @Override
        public int getItemCount() {
            return colors.length;
        }

        public class ColorPickerViewHolder extends RecyclerView.ViewHolder {
            public LinearLayout linearLayout;
            public ImageView colorBox;

            public ColorPickerViewHolder(View itemView) {
                super(itemView);
                linearLayout = (LinearLayout) itemView;
                colorBox = linearLayout.findViewById(R.id.ColorBox);
            }
        }

        public void setSelected(int selected) {
            this.selected = selected;
            notifyDataSetChanged();
        }
    }
}
