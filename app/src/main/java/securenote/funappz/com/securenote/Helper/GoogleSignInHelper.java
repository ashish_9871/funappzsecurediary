package securenote.funappz.com.securenote.Helper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by ashish on 21/1/18.
 */

public class GoogleSignInHelper {
    public static final int RC_SIGN_IN = 200;
    private Context context;
    private GoogleSignInClient googleSignInClient;
    private static GoogleSignInHelper googleSignInHelper;
    private GoogleSignInAccount googleSignInAccount;
    private GoogleSignInOptions googleSignInOptions;
    public interface SignInEvents {
        void startSignInActivity(GoogleSignInHelper googleSignInHelper, Intent signInIntent, int requestCode);
    }

    public interface SignInCompletedEvents {
        void onSuccess(GoogleSignInHelper googleSignInHelper, GoogleSignInAccount googleSignInAccount);

        void onFailure(GoogleSignInHelper googleSignInHelper, Exception e);
    }

    public static GoogleSignInHelper getInstance(Context context) {
        googleSignInHelper = new GoogleSignInHelper(context);
        return googleSignInHelper;
    }

    private GoogleSignInHelper(Context context) {
        this.context = context;
        this.googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(Drive.SCOPE_FILE)
                .build();
        this.googleSignInClient = GoogleSignIn.getClient(context, googleSignInOptions);
    }

    public GoogleSignInAccount getLastSignInAccount() {
        return GoogleSignIn.getLastSignedInAccount(context);
    }

    public GoogleSignInClient getGoogleSignInClient() {
        return googleSignInClient;
    }

    public void GoogleSignIn(@NonNull SignInEvents signInEvents) {
        Intent signInIntent = getGoogleSignInClient().getSignInIntent();
        signInEvents.startSignInActivity(this, signInIntent, RC_SIGN_IN);
    }

    public void GoogleSilentSignIn(@NonNull final SignInCompletedEvents signInCompletedEvents) {
        getGoogleSignInClient().silentSignIn().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                signInCompletedEvents.onFailure(googleSignInHelper, new Exception(e.hashCode() + e.getMessage()));
            }
        }).addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
            @Override
            public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                signInCompletedEvents.onSuccess(googleSignInHelper,googleSignInAccount);
            }
        });
    }

    public GoogleSignInHelper GoogleSignOut() {
        getGoogleSignInClient().signOut();
        return this;
    }

    public void handleSignInResult(int requestCode, int resultCode, Intent data, @NonNull SignInCompletedEvents signInCompletedEvents) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> signInTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount googleSignInAccount = signInTask.getResult(ApiException.class);
                signInCompletedEvents.onSuccess(this, googleSignInAccount);
            } catch (Exception e) {
                signInCompletedEvents.onFailure(this, e);
            }

        }
    }

    public int getStatusCode(String code){
        String a[] = code.split(":");
        if(a.length>0){
            try {
                return Integer.parseInt(a[0]);
            }catch (Exception e){
                return 0;
            }
        }
        return 0;
    }
}
