package securenote.funappz.com.securenote.Core;

import android.content.Context;
import android.view.View;

/**
 * Created by ashish on 5/1/18.
 */

public interface IBaseView extends View.OnClickListener{
    Context getAppContext();
    void onSuccess(String msg);
    void onFailure(String msg);
}
