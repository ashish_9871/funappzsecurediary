package securenote.funappz.com.securenote;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by ashish on 30/12/17.
 */

public class BgRunnableTask extends Thread {

    public Handler handler;

    public interface BackgroundProcessable {
        public void execute();

        public void onComplete();
    }

    public BackgroundProcessable backgroundProcessable;

    public BgRunnableTask(BackgroundProcessable backgroundProcessable) {
        this.backgroundProcessable = backgroundProcessable;
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void run() {
        backgroundProcessable.execute();
        handler.post(new Runnable() {
            @Override
            public void run() {
                backgroundProcessable.onComplete();
            }
        });
    }
}
