package securenote.funappz.com.securenote.Main;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import securenote.funappz.com.securenote.BgRunnableTask;
import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DriveBackupHelper;
import securenote.funappz.com.securenote.Helper.GoogleSignInHelper;
import securenote.funappz.com.securenote.Helper.SettingsHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.R;

/**
 * Created by ashish on 5/2/18.
 */

public class RestoreService extends Service {
    private AppDBHelper appDBHelper;
    private SettingsHelper settingsHelper;
    private GoogleSignInHelper googleSignInHelper;
    private DriveBackupHelper driveBackupHelper;
    private static int FC_CODE = 7000;
    private static int RESTORE_GAP_MS = 60 * 60 * 1000 * 1; // 2 hour

    @Override
    public void onCreate() {
        super.onCreate();
        appDBHelper = new AppDBHelper(this);
        settingsHelper = SettingsHelper.getInstance(this);
        googleSignInHelper = GoogleSignInHelper.getInstance(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        try {
            BgRunnableTask restoreTask = new BgRunnableTask(new BgRunnableTask.BackgroundProcessable() {
                @Override
                public void execute() {
                    try {
                        long lastRestore = settingsHelper.getLastRestore();
                        long now = System.currentTimeMillis();
                        final Intent intentSuccess = new Intent(GlobalReceiver.ACTION_RESTORE_SUCCESS);
                        final Intent intentFailure = new Intent(GlobalReceiver.ACTION_RESTORE_FAILED);
                        if ((now - lastRestore) < RESTORE_GAP_MS) {
                            Thread.sleep(5000);
                            String data = UtilityHelper.getLocalBackup(getApplicationContext());
                            restoreNotes(data, intentSuccess, intentFailure);
                        } else {
                            driveBackupHelper = DriveBackupHelper.getInstance(getApplicationContext(), googleSignInHelper.getLastSignInAccount());
                            driveBackupHelper.readBackupFile(new DriveBackupHelper.BackupFetchEvents() {

                                @Override
                                public void onFetchSuccess(DriveBackupHelper driveBackupHelper, String jsonString) {
                                    settingsHelper.setLastRestore(System.currentTimeMillis());
                                    UtilityHelper.storeLocalBackup(getApplicationContext(), jsonString);
                                    restoreNotes(jsonString, intentSuccess, intentFailure);
                                }

                                @Override
                                public void onFetchFailure(DriveBackupHelper driveBackupHelper, Exception e) {
                                    String msg = "Error restoring notes:" + e.getMessage();
                                    if (settingsHelper.isAccountSetup()) {
                                        msg = "No restorable backup found.";
                                        intentSuccess.putExtra(GlobalReceiver.ACTION_RESTORE_SUCCESS, msg);
                                        getApplicationContext().sendBroadcast(intentSuccess);
                                        showNotification("Restore Success", msg);
                                        return;
                                    }
                                    intentFailure.putExtra(GlobalReceiver.ACTION_RESTORE_FAILED, msg);
                                    getApplicationContext().sendBroadcast(intentFailure);
                                    showNotification("Restore Failed", msg);
                                }
                            }, false);
                        }
                    } catch (Exception e) {

                    }
                    stopSelf();
                }

                @Override
                public void onComplete() {

                }
            });
            restoreTask.start();

        } catch (Exception e) {
            UtilityHelper.Log(e.getMessage());
        }
        return START_NOT_STICKY;
    }

    private void restoreNotes(String jsonString, final Intent intentSuccess, Intent intentFailure) {
        jsonString = (jsonString == null ? "" : jsonString);
        appDBHelper.syncNotes(jsonString, new AppDBHelper.NotesSyncEvents() {
            @Override
            public void onSuccess(int totalSynced) {
                getApplicationContext().sendBroadcast(new Intent(GlobalReceiver.ACTION_NOTE_LIST));
                String msg = totalSynced + " notes are restored.";
                intentSuccess.putExtra(GlobalReceiver.ACTION_RESTORE_SUCCESS, msg);
                showNotification("Restore Success", msg);
                getApplicationContext().sendBroadcast(intentSuccess);
            }

            @Override
            public void onFailure(Exception e) {
                String msg = "0 notes restored.";
                intentSuccess.putExtra(GlobalReceiver.ACTION_RESTORE_SUCCESS, msg);
                getApplicationContext().sendBroadcast(intentSuccess);
                showNotification("Restore Success", msg);
            }
        });
    }

    private void showNotification(String title, String msg) {
        android.support.v4.app.NotificationCompat.Builder mBuilder =
                new android.support.v4.app.NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_sync_black_24dp);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(FC_CODE, mBuilder.build());
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }


}
