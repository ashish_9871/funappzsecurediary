package securenote.funappz.com.securenote.Main.Entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import securenote.funappz.com.securenote.Helper.AppDBHelper;

/**
 * Created by ashish on 7/11/17.
 */

public class Note implements Serializable {

    private int id;
    private String label;
    private String date;
    private String note;

    public Note() {
    }

    public Note(String label, String date, String note) {
        this.label = label;
        this.date = date;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public String getIdValue() {
        return id + "";
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDate() {
        return date;
    }

    /**
     *
     * @param date date string Y-m-d format
     */
    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", date='" + date + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = null;
        try {
            jsonObject=new JSONObject();
            jsonObject.put(AppDBHelper.KEY_ID,getId());
            jsonObject.put(AppDBHelper.KEY_DATE,getDate());
            jsonObject.put(AppDBHelper.KEY_LABEL,getLabel());
            jsonObject.put(AppDBHelper.KEY_NOTE,getNote());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}
