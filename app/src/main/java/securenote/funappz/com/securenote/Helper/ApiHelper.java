package securenote.funappz.com.securenote.Helper;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import securenote.funappz.com.securenote.Constants;

/**
 * Created by ashish on 29/12/17.
 */

public class ApiHelper {
    private static String apiBaseUrl = "http://funappz.co.in/api/v1";
    public static final String RESPONSE_KEY_STATUS = "status";
    public static final String RESPONSE_KEY_DATA = "data";
    public static final String RESPONSE_KEY_ERROR = "error";
    public static final String RESPONSE_KEY_SUCCESS = "success";
    public static final String RESPONSE_KEY_MESSAGE = "message";
    public static final String RESPONSE_KEY_ERROR_STRING = "error_string";
    private static ApiHelper apiHelper;

    private JSONObject responseObject;

    public static ApiHelper getInstance() {
        if (apiHelper == null) {
            apiHelper = new ApiHelper();
        }
        return apiHelper;
    }

    private String getApiBaseUrl() {
        return apiBaseUrl;
    }

    public JSONObject sendForgetPasswordRequest(String email, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            jsonObject.put("key", Constants.DPAPKEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = getApiBaseUrl() + "/diary/password";
        HttpRequest request = new HttpRequest(url, HttpRequest.METHOD_POST);

        request.useCaches(false);
        request.contentType(HttpRequest.CONTENT_TYPE_JSON);
        request.accept(HttpRequest.CONTENT_TYPE_JSON);
        request.send(jsonObject.toString());
        String response = request.body();
        try {
            responseObject = new JSONObject(response);
        } catch (JSONException e) {
            responseObject = null;
            e.printStackTrace();
            UtilityHelper.Log("Response:" + response);
            UtilityHelper.Log("Response Error:" + e.getMessage());
        }
        return responseObject;
    }

    public JSONObject getResponseObject() {
        return responseObject;
    }

    public boolean isSuccess() {
        if (responseObject == null) {
            return false;
        }
        return responseObject.optString(ApiHelper.RESPONSE_KEY_STATUS, "error").toString().equalsIgnoreCase(ApiHelper.RESPONSE_KEY_SUCCESS);
    }

    public String getMessage() {
        if (responseObject == null) {
            return "HTTP Request Error";
        }
        return responseObject.optString(ApiHelper.RESPONSE_KEY_MESSAGE);
    }

    public String getErrorDescription() {
        if (responseObject == null) {
            return "Unable to process, please try again after sometime.";
        }
        return responseObject.optString(ApiHelper.RESPONSE_KEY_ERROR_STRING);
    }

    public HashMap<String, String> getErrors() {
        HashMap<String, String> errors = new HashMap<String, String>();
        if (responseObject == null) {
            return errors;
        }
        JSONObject jsonErrorObject = responseObject.optJSONObject(ApiHelper.RESPONSE_KEY_ERROR);
        if (jsonErrorObject != null) {
            Iterator<String> iterator = jsonErrorObject.keys();
            while (iterator.hasNext()) {
                String errorField = iterator.next();
                JSONArray fieldErrors = jsonErrorObject.optJSONArray(errorField);
                List<String> errorTexts = new ArrayList<String>();
                for (int i = 0; i < fieldErrors.length(); i++) {
                    errorTexts.add(fieldErrors.opt(i).toString());
                }
                errors.put(errorField, TextUtils.join("\n", errorTexts));
            }
        }
        return errors;
    }

    public JSONObject getData() {
        if (responseObject == null) {
            return null;
        }
        return responseObject.optJSONObject(ApiHelper.RESPONSE_KEY_DATA);
    }
}
