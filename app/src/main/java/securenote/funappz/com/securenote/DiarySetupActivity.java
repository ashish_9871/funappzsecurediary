package securenote.funappz.com.securenote;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import securenote.funappz.com.securenote.Main.MainAppActivity;

public class DiarySetupActivity extends BaseActivity {

    private EditText emailInput;
    private EditText passwordInput;
    private TextView logoText;
    private TextView setupHelpText;
    private Button setupBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(isDiarySetup()){
            onSuccess(null);
        }
        setContentView(R.layout.activity_diary_setup);
        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        logoText = findViewById(R.id.logoText);
        setupHelpText = findViewById(R.id.setupHelpText);
        setupBtn = findViewById(R.id.setupBtn);

        logoText.setTypeface(TypeFacepacifico);
        setupHelpText.setTypeface(TypeFacetangerine);
        setupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveDiaryAction();
            }
        });
    }

    private void saveDiaryAction() {
        // hide keyboard
        String email = emailInput.getText().toString().trim();
        String password = passwordInput.getText().toString().trim();
        if (email.isEmpty()) {
            emailInput.setError("Email is required.");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailInput.setError("Email address is invalid.");
        } else if (password.isEmpty()) {
            passwordInput.setError("Password is required.");
        } else if (password.length() < 4) {
            passwordInput.setError("Password should be atleast 4 characters long.");
        } else {
            // all ok save
            emailInput.clearFocus();
            passwordInput.clearFocus();
            hideKeyBoard();
            getSettingsHelper().setAppKey(password);
            getSettingsHelper().setEmail(email);
            onSuccess("Password successfully saved.");
        }
    }

    private void onSuccess(String msg) {
        Intent passwordSetup = new Intent(getApplicationContext(), MainAppActivity.class);
        if (msg != null) {
            passwordSetup.setAction(GlobalReceiver.ACTION_PASSWORD_SETUP);
            showToast(msg, false);
        }
        startActivity(passwordSetup);
        finish();
    }


}
