package securenote.funappz.com.securenote.Core;

/**
 * Created by ashish on 5/1/18.
 */

public interface IBasePresenter  {
    void onStart();
    void onStop();
}
