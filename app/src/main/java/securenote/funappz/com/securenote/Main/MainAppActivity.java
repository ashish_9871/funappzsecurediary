package securenote.funappz.com.securenote.Main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import securenote.funappz.com.securenote.AskPasswordActivity;
import securenote.funappz.com.securenote.BaseActivity;
import securenote.funappz.com.securenote.Constants;
import securenote.funappz.com.securenote.DiarySetupActivity;
import securenote.funappz.com.securenote.GlobalReceiver;
import securenote.funappz.com.securenote.Helper.AppDBHelper;
import securenote.funappz.com.securenote.Helper.DateHelper;
import securenote.funappz.com.securenote.Helper.GoogleSignInHelper;
import securenote.funappz.com.securenote.Helper.SettingsHelper;
import securenote.funappz.com.securenote.Helper.UtilityHelper;
import securenote.funappz.com.securenote.Main.Entity.Note;
import securenote.funappz.com.securenote.Main.Fragment.HomeFragment;
import securenote.funappz.com.securenote.Main.Fragment.NoteAddFragment;
import securenote.funappz.com.securenote.R;
import securenote.funappz.com.securenote.customview.RichEditText;

import static securenote.funappz.com.securenote.GlobalReceiver.ACTION_NOTE_LIST;


public class MainAppActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentTransaction fragmentTransaction;
    private HomeFragment homeFragment;
    private NoteAddFragment noteAddFragment;
    private SettingsHelper settingsHelper;
    private String userAppKey = null;
    private boolean firstLaunch = false;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private NotesBroadCastListener notesBroadCastListener;
    private TextView lastBackupView;
    private RichEditText dummyRichText;
    private Switch passwordProtectionToggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_app);
        dummyRichText = new RichEditText(this);
        notesBroadCastListener = new NotesBroadCastListener();
        homeFragment = new HomeFragment();
        noteAddFragment = new NoteAddFragment();
        settingsHelper = SettingsHelper.getInstance(this);
        userAppKey = settingsHelper.getAppKey();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        if (navigationView.getHeaderCount() > 0) {
            lastBackupView = navigationView.getHeaderView(0).findViewById(R.id.last_backup_time);
            passwordProtectionToggleButton = navigationView.getHeaderView(0).findViewById(R.id.passwordProtectionToggleButton);
            passwordProtectionToggleButton.setChecked(!isAllowedNoPassword());
            passwordProtectionToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean status) {
                    getSettingsHelper().setAllowedNoPasswordLogin(!status);
                }
            });

        }
        if (getIntent().getAction() != GlobalReceiver.ACTION_PASSWORD_SETUP && userAppKey == null) {
            // either settings not saved or broadcast is not save password go to password setup screen
            startActivity(new Intent(this, DiarySetupActivity.class));
            finish();
        } else if (getIntent().getAction() == GlobalReceiver.ACTION_PASSWORD_SETUP) {
            firstLaunch = true;
        } else if (getIntent().getAction() == GlobalReceiver.ACTION_PASSWORD_VERIFIED) {
            firstLaunch = true;
        }
    }

    @Override
    public void onBackPressed() {
        exitOnDoubleBackPress();
    }


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GlobalReceiver.ACTION_NOTE_ADD);
        intentFilter.addAction(GlobalReceiver.ACTION_NOTE_EDIT);
        intentFilter.addAction(GlobalReceiver.ACTION_NOTE_SHARE);
        intentFilter.addAction(GlobalReceiver.ACTION_NOTE_COPY);
        intentFilter.addAction(GlobalReceiver.ACTION_NOTE_LIST);
        intentFilter.addAction(GlobalReceiver.ACTION_BACKUP_SUCCESS);
        intentFilter.addAction(GlobalReceiver.ACTION_BACKUP_FAILED);
        intentFilter.addAction(GlobalReceiver.ACTION_GOOGLE_ACCOUNT_GET_FAILED);
        intentFilter.addAction(GlobalReceiver.ACTION_RESTORE_FAILED);
        intentFilter.addAction(GlobalReceiver.ACTION_RESTORE_SUCCESS);
        registerReceiver(notesBroadCastListener, intentFilter);
        if (!firstLaunch && !isAllowedNoPassword()) {
            askPasswordAction();
            finish();
            return;
        }
        if (lastBackupView != null) {
                lastBackupView.setText(getSettingsHelper().getLastBackupStr());
        }
        showHomeFragment(null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(notesBroadCastListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_app, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!getSettingsHelper().isAccountSetup()) {
            MenuItem warning = menu.add("Backup Account not Setup.");
            warning.setIcon(android.R.drawable.stat_sys_warning);
            warning.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
            warning.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    buildConfirmDialog("Continue", "Continue to choose google drive backup account.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                            getGoogleSignInHelper().getGoogleSignInClient().signOut();
                            getGoogleSignInHelper().GoogleSignIn(new GoogleSignInHelper.SignInEvents() {
                                @Override
                                public void startSignInActivity(GoogleSignInHelper googleSignInHelper, Intent signInIntent, int requestCode) {
                                    startActivityForResult(signInIntent, requestCode);
                                }
                            });
                        }
                    }, null);

                    return true;
                }
            });
        }
        return true;
    }

    public void askPasswordAction() {
        Intent intentAskPassword = new Intent(this, AskPasswordActivity.class);
        startActivity(intentAskPassword);
    }

    public void showHomeFragment(Intent intent) {
        homeFragment = new HomeFragment();
        setActiveFragment(homeFragment, intent);
    }

    public void showNoteAddNoteFragment(Intent intent) {
        noteAddFragment = new NoteAddFragment();
        setActiveFragment(noteAddFragment, intent);
    }


    public void setActiveFragment(Fragment fragment, Intent intent) {
        hideKeyBoard();
        if (intent != null) {
            fragment.setArguments(intent.getExtras());
        }
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.fadein, R.animator.fadeout);
        fragmentTransaction.replace(R.id.contentFrame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GoogleSignInHelper.RC_SIGN_IN) {
            if (resultCode == RESULT_CANCELED) {
                showSnakeBar("You cancelled the operation.", true);
            }
            getGoogleSignInHelper().handleSignInResult(requestCode, resultCode, data, new GoogleSignInHelper.SignInCompletedEvents() {
                @Override
                public void onSuccess(GoogleSignInHelper googleSignInHelper, GoogleSignInAccount googleSignInAccount) {
                    showToast("success", true);
                    init();
                    getSettingsHelper().setAccountSetup(true);
                    invalidateOptionsMenu();
                    buildConfirmDialog("Yes", "Do you want to check/restore diary notes from google drive?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            restoreAction();
                        }
                    }, null);
                }

                @Override
                public void onFailure(GoogleSignInHelper googleSignInHelper, Exception e) {
                    showSnakeBar("Operation failed: " + e.getMessage(), true);
                    UtilityHelper.Log("failed:" + e.getMessage());
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                showHomeFragment(null);
                break;
            case R.id.action_settings:
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.action_backup:
                final GoogleSignInAccount account = getGoogleSignInHelper().getLastSignInAccount();
                if (account != null) {
                    buildConfirmDialog("Change Account", "Backup settings are already completed with gmail account: \n\n" + account.getEmail() + "\n", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getGoogleSignInHelper().getGoogleSignInClient().signOut();
                            onOptionsItemSelected(item);
                            return;
                        }
                    }, null);
                } else {
                    getGoogleSignInHelper().GoogleSignIn(new GoogleSignInHelper.SignInEvents() {
                        @Override
                        public void startSignInActivity(GoogleSignInHelper googleSignInHelper, Intent signInIntent, int requestCode) {
                            startActivityForResult(signInIntent, requestCode);
                        }
                    });
                }
                break;
            case R.id.action_restore:
                restoreAction();
                break;
            case R.id.action_share:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, Constants.playStoreUrl);
                startActivity(shareIntent);
                break;
            case R.id.action_help:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.HelpUrl));
                startActivity(browserIntent);
                break;
        }
        return true;
    }

    private void restoreAction() {
        showSnakeBar("Restoring notes from last backup.", true);
        startService(new Intent(getApplicationContext(), RestoreService.class));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        onOptionsItemSelected(item);
        return true;
    }


    private class NotesBroadCastListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                switch (intent.getAction()) {
                    case ACTION_NOTE_LIST:
                        showHomeFragment(null);
                        break;
                    case GlobalReceiver.ACTION_NOTE_ADD:
                        showNoteAddNoteFragment(null);
                        break;
                    case GlobalReceiver.ACTION_NOTE_EDIT:
                        showNoteAddNoteFragment(intent);
                        break;
                    case GlobalReceiver.ACTION_NOTE_COPY:
                        intent.putExtra(GlobalReceiver.ACTION_NOTE_COPY, true);
                        showNoteAddNoteFragment(intent);
                        break;
                    case GlobalReceiver.ACTION_NOTE_SHARE:
                        UtilityHelper.Log("hre");
                        intent.putExtra(GlobalReceiver.ACTION_NOTE_SHARE, true);
                        String id = intent.getIntExtra(AppDBHelper.KEY_ID+"",0)+"";
                        Note note = getAppDBHelper().getNoteBy(AppDBHelper.KEY_ID,id);
                        if(note==null){
                            showSnakeBar("Can't share, Invalid note.",true);
                            return;
                        }

                        dummyRichText.setHtml(note.getNote());
                        String textToShare = note.getLabel()+"\n";
                        textToShare+=dummyRichText.getText().toString()+"\n";
                        textToShare+="Created :"+note.getDate();
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
                        startActivity(shareIntent);
                        break;
                    case GlobalReceiver.ACTION_BACKUP_SUCCESS:
                        showSnakeBar("Backup Success", true);
                        invalidateOptionsMenu();
                        break;
                    case GlobalReceiver.ACTION_BACKUP_FAILED:
                    case GlobalReceiver.ACTION_GOOGLE_ACCOUNT_GET_FAILED:
                        getGoogleSignInHelper().getGoogleSignInClient().signOut();
                        showSnakeBar("Backup failed", false);
                        invalidateOptionsMenu();
                        break;
                    case GlobalReceiver.ACTION_RESTORE_FAILED:
                        showSnakeBar(intent.getStringExtra(GlobalReceiver.ACTION_RESTORE_FAILED), true);
                        break;
                    case GlobalReceiver.ACTION_RESTORE_SUCCESS:
                        showSnakeBar(intent.getStringExtra(GlobalReceiver.ACTION_RESTORE_SUCCESS), true);
                        break;
                }
            }
        }
    }
}
