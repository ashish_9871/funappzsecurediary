package securenote.funappz.com.securenote.customview;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import securenote.funappz.com.securenote.system.HtmlTagHandler;

/**
 * Created by ashish on 18/11/17.
 */

public class RichTextView extends EmojiconTextView {

    public RichTextView(Context context) {
        super(context);
    }

    public RichTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RichTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setHtml(String htmlText) {
        if (Build.VERSION.SDK_INT < 24) {
            Spanned spanned = Html.fromHtml(htmlText, null, new HtmlTagHandler());
            setText(spanned);
        } else {
            Spanned spanned = Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY, null, new HtmlTagHandler());
            setText(spanned);
        }
    }

    public void setPlainText(String text){
        setHtml(text);
        setText(getText().toString());
    }
}
