package securenote.funappz.com.securenote.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ashish on 4/1/18.
 */

public class DateHelper {
    public static final String DATE_FORMAT_HUMAN = "dd MMM yyyy";
    public static final String DATE_TIME_FORMAT_HUMAN = "dd MMM yyyy hh:mm a";
    public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
    private Date date;
    private int day = 0;
    private int month = 0;
    private int year = 0;
    private String monthName = "NA";
    private String dayName = "NA";
    private DateHelper dateHelper;

    public static DateHelper getInstance(Date date) {
        return new DateHelper(date);
    }

    public static DateHelper getInstance(String dateString) {
        return new DateHelper(dateString);
    }

    public static DateHelper getInstance(int day, int month, int year) {
        // fix 0 based month
        if (month < 12) {
            month += 1;
        }
        return new DateHelper(year + "-" + month + "-" + day);
    }

    public static DateHelper getInstance(long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        return DateHelper.getInstance(calendar.getTime());
    }

    public static DateHelper getInstance() {
        return new DateHelper(new Date());
    }

    private DateHelper() {

    }

    private DateHelper(Date date) {
        this.date = (date == null ? new Date() : date);
        setDateParts(getDate());
    }

    private DateHelper(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_YMD, Locale.ENGLISH);
        try {
            this.date = format.parse(dateString);
            setDateParts(getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setDateParts(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        year = calendar.get(Calendar.YEAR);
        monthName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH);
        dayName = calendar.getDisplayName(Calendar.DAY_OF_MONTH, Calendar.SHORT, Locale.ENGLISH);
    }

    public String getDay() {
        return day + "";
    }

    public int getDayInt() {
        return day;
    }

    public String getMonth() {
        return month + "";
    }

    public int getMonthInt() {
        return month;
    }

    public String getYear() {
        return year + "";
    }

    public int getYearInt() {
        return year;
    }

    public String getMonthName() {
        return monthName;
    }

    public String getDayName() {
        return dayName;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return getDate().toString();
    }

    public String format(String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.format(getDate());
        } catch (Exception e) {
            return getDate().toString();
        }
    }
}
